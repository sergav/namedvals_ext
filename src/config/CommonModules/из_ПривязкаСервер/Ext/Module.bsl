﻿// Полезная нагрузка в серверном контексте
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возращает значение привязки
//
// Параметры: 
//	Идентификатор - Строка - Идентификатор привязки
//
// Возвращаемое значение: 
//	Произвольный
//
Функция Получить(Идентификатор) Экспорт	
	Возврат Коллекция().Получить(ПолучитьКлюч(Идентификатор));
КонецФункции // Получить

// Устанавливает значение привязки. Возвращает значение на момент до выполнения метода
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор привязки
//	Коллекция - Соотетсвие - Коллекция привязок
//	Значение - Произвольный - Новое значение	
// Возвращаемое значение: 
// 	Произвольный
//
Функция Установить(Идентификатор, Коллекция, Значение = Null) Экспорт
	
	из_ПривязкаПрава.АдминистрированиеОтсутствуетИсключение();
	
	Ключ = ПолучитьКлюч(Идентификатор);
	
	Результат = из_Модуль.ом_Коллекция().СоответствиеСвойство(Коллекция, Ключ, Значение);
	
	УстановитьПривилегированныйРежим(Истина);
	
	КлючКоллекции = КоллекцияИмя();
	
	НачатьТранзакцию();

	Попытка
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Коллекция
		, Неопределено
		, ПользовательИмя());
		
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ВызватьИсключение ОписаниеОшибки();
	КонецПопытки;
	
	Возврат Результат;
	
КонецФункции // Установить

// Добавляет привязку в коллекцию именованных значений.
//
// Параметры:
//	Идентификатор - Строка - Идентификатор привязки
//  Информация - Структура - Дополнительная информация привязки
//  Значение - Произвольное
//
Процедура Добавить(Идентификатор, Информация, Значение = Null) Экспорт
	
	из_ПривязкаПрава.АдминистрированиеОтсутствуетИсключение();
	
	Ключ = НРег(Идентификатор);
	
	НачатьТранзакцию();
	
	Попытка
		
		// Добавляем значение привязки 
		Коллекция = Коллекция();
		Коллекция.Вставить(Ключ, Значение);
		
		КлючКоллекции = КоллекцияИмя();
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Коллекция
		, Неопределено
		, ПользовательИмя());
		
		// Добавляем вспомогательную инормацию привязки	
		Коллекция = Информация();
		Информация.Ключ = Ключ;
		Коллекция.Вставить(Ключ, Информация);
		
		КлючКоллекции = ИнформацияИмя();
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Коллекция
		, Неопределено
		, ПользовательИмя());
		
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ВызватьИсключение ОписаниеОшибки();
	КонецПопытки;
	
КонецПроцедуры // Добавить

// Удаляет привязку
//
// Параметры:
//	Идентификатор - Строка - Идентификатор привязки
//  Коллекция - Соответствие - Коллекция привязок
//  Информация - Соответствие - Коллекция вспомогательной информации привязок
//
Процедура Удалить(Идентификатор, Коллекция, Информация) Экспорт
	
	из_ПривязкаПрава.АдминистрированиеОтсутствуетИсключение();
	
	УстановитьПривилегированныйРежим(Истина);
	
	Ключ = ПолучитьКлюч(Идентификатор);
	
	НачатьТранзакцию();
	
	Попытка	
		
		// Удаляем значение привязки
		Коллекция.Удалить(Ключ);
		КлючКоллекции = КоллекцияИмя();
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Коллекция
		, Неопределено
		, ПользовательИмя());
		
		// Удаляем вспомогательную информацию привязки
		Информация.Удалить(Ключ);	
		КлючКоллекции = ИнформацияИмя();
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Информация
		, Неопределено
		, ПользовательИмя());
		
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ВызватьИсключение ОписаниеОшибки();
	КонецПопытки;
	
КонецПроцедуры // Удалить 

// Возвращает вспомогательную информацию привязки
//
// Параметры: 
//	Идентификатор - Строка - Идентификатор привязки
//
// Возвращаемое значение: 
// 	Структура
//
Функция ПолучитьИнформацию(Идентификатор) Экспорт	
	Возврат Информация().Получить(ПолучитьКлюч(Идентификатор));	
КонецФункции // ПолучитьИнформацию

// Сохраняет вспомогательную информацию привязки
//
// Параметры: 
//	Идентификатор - Строка - Идентификатор привязки
// 	Информация - Структура - Дополнительная информация привязки
//
Процедура СохранитьИнформацию(Идентификатор, Информация) Экспорт
	
	из_ПривязкаПрава.АдминистрированиеОтсутствуетИсключение();
		
	УстановитьПривилегированныйРежим(Истина);
	
	НачатьТранзакцию();

	Попытка
		
		Ключ = ПолучитьКлюч(Идентификатор);
		Информация.Ключ = Ключ;
		Коллекция = Информация();
		Коллекция.Вставить(Ключ, Информация);
		
		КлючКоллекции = ИнформацияИмя();
		
		ХранилищеСистемныхНастроек.Сохранить(КлючКоллекции
		, КлючКоллекции
		, Коллекция
		, Неопределено
		, ПользовательИмя());
		
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ВызватьИсключение ОписаниеОшибки();
	КонецПопытки;
		
КонецПроцедуры // СохранитьИнформацию

// Возвращает коллекцию привязок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Соответствие
//
Функция Коллекция() Экспорт
	Возврат ПолучитьКоллекциюНастроек(КоллекцияИмя());
КонецФункции // Коллекция 

// Возвращает коллекцию дополнительной информации привязок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Соответствие
//
Функция Информация() Экспорт	
	Возврат ПолучитьКоллекциюНастроек(ИнформацияИмя());
КонецФункции // Информация

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает ключ привязки
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор привязки
//
// Возвращаемое значение: 
// 	Строка
//
Функция ПолучитьКлюч(Идентификатор) Экспорт	
	Возврат НРег(Идентификатор);	
КонецФункции // ПолучитьКлюч

// Возвращает коллекцию из настроек
//
// Параметры: 
// 	КлючКоллекции - Строка - Ключ настройки
// Возвращаемое значение: 
// 	Соотетсвие
//
Функция ПолучитьКоллекциюНастроек(КлючКоллекции)
	
	УстановитьПривилегированныйРежим(Истина);
		
	Результат = ХранилищеСистемныхНастроек.Загрузить(КлючКоллекции
	, КлючКоллекции
	, Неопределено
	, ПользовательИмя());
	
	Возврат ?(из_Модуль.ом_Значение().СоответствиеЭто(Результат), Результат, Новый Соответствие);
	
КонецФункции // ПолучитьКоллекциюНастроек

// Возвращает имя фиктивного пользователя хранения настроек
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	Строка
// 
Функция ПользовательИмя()
	
	Возврат "36592493-f035-417a-b67a-259ca30f55f5";
	
КонецФункции // ПользовательИмя 

// Возвращает имя коллекции привязок
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	Строка
// 
Функция КоллекцияИмя()
	
	Возврат "8ba57883-6879-4ae0-994f-bee188698bd4";
	
КонецФункции // КоллекцияИмя

// Возвращает имя коллекции вспомогательной информации привязок
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	Строка
// 
Функция ИнформацияИмя()
	
	Возврат "7dcc0448-3a78-4754-9606-14d0bf9d6669";
	
КонецФункции // ИнформацияИмя 

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
