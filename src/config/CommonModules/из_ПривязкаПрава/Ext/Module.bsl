﻿// Проверка прав на доступ к администрированию именованных значений
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Вызов исключения при недоступности администрирования именованных значений
// 
// Параметры: 
// 
Процедура АдминистрированиеОтсутствуетИсключение() Экспорт
	
	Если НЕ из_ПривязкаПовтИсп.РольАдминистрированиеДоступна() 
		И НЕ ПривилегированныйРежим()
		Тогда
		ВызватьИсключение из_ПривязкаТексты.ПраваАдминистрированиеОтсутствует();
	КонецЕсли;
	
КонецПроцедуры // АдминистрированиеОтсутствуетИсключение 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
