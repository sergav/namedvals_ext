﻿// Кэш внешних относительно расширения модулей
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает общий модуль ом_СредаВыполнения
//
// Параметры: 
//
// Возвращаемое значение: 
//
Функция ом_СредаВыполнения() Экспорт
	
	Возврат Модуль("ом_СредаВыполнения");
	
КонецФункции // ом_СредаВыполнения 

// Возвращает общий модуль ом_Значение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Значение() Экспорт
	
	Возврат Модуль("ом_Значение");
	
КонецФункции // ом_Значение 

// Возвращает общий модуль ом_Подсистема
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Подсистема() Экспорт
	
	Возврат Модуль("ом_Подсистема");
	
КонецФункции // ом_Подсистема 

// Возвращает общий модуль ом_Коллекция
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Коллекция() Экспорт
	
	Возврат Модуль("ом_Коллекция");
	
КонецФункции // ом_Коллекция 

// Возвращает общий модуль ом_АдаптерАктивный
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_АдаптерАктивный() Экспорт
	
	Возврат Модуль("ом_АдаптерАктивный");
	
КонецФункции // ом_АдаптерАктивный 

// Возвращает общий модуль ом_АадптерРегион
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_АдаптерРегион() Экспорт
	
	Возврат Модуль("ом_АдаптерРегион");
	
КонецФункции // ом_АдаптерРегион 

// Возвращает общий модуль ом_ДиалогКЛиент
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_ДиалогКлиент() Экспорт
	
	Возврат Модуль("ом_ДиалогКлиент");
	
КонецФункции // ом_ДиалогКлиент 

// Возвращает общий модуль ом_Строка
//
// Параметры: 
//
// Возвращаемое значение: 
//  ОбщийМодуль
//
Функция ом_Строка() Экспорт
	
	Возврат Модуль("ом_Строка");
	
КонецФункции // ом_Строка

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает общий модуль по имени
//
// Параметры: 
// 	Имя - Строка - Имя общего модуля
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция Модуль(Имя) 
	
	// BSLLS:ExecuteExternalCodeInCommonModule-off - имена модулей жестко заданы в вызывающих методах
	Возврат Вычислить(Имя);
	// BSLLS:ExecuteExternalCodeInCommonModule-on
	
КонецФункции // Модуль 

#КонецОбласти
